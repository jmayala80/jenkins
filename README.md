# Jenkins
- Jenkins: https://hub.docker.com/r/jenkins/jenkins
- Jenkins LTS: https://www.jenkins.io/download/lts/

To use the latest LTS:    $ docker pull jenkins/jenkins:lts  
To use the latest weekly: $ docker pull jenkins/jenkins  

Lighter alpine based image also available  

Doc: - https://github.com/jenkinsci/docker/blob/master/README.md

# Automatizar la creación de Jenkins
## Docker build
```
docker build -t jamarcos02/jenkins:${TAG} .
```
## Docker run
```
docker run --rm --name ${CONTAINER_NAME} -p 8080:8080 -p 50000:50000 jamarcos02/jenkins:${TAG}
```

## Setear configuración del proxy para el container
```
ENV HTTP_PROXY=${PROXY_HOST:PORT}
ENV HTTPS_PROXY=${PROXY_HOST:PORT}
ENV FTP_PROXY=${PROXY_HOST:PORT}
ENV SFTP_PROXY=${PROXY_HOST:PORT}
ENV http_proxy=${PROXY_HOST:PORT}
ENV https_proxy=${PROXY_HOST:PORT}
ENV ftp_proxy=${PROXY_HOST:PORT}
ENV sftp_proxy=${PROXY_HOST:PORT}
ENV NO_PROXY=${NO_PROXY1},${NO_PROXY2} ... ${NO_PROXYX}
```
## Setear la configuración del proxy para Jenkins
```
JAVA_OPTS: '-Dhttp.proxyHost=${PROXY_HOST} -Dhttp.proxyPort=${PORT} -Dhttps.proxyHost=${PROXY_HOST} -Dhttps.proxyPort=${PORT} -Dhttp.nonProxyHosts=${NO_PROXY1}|127.0.0.0|127.0.1.1|${NO_PROXY2}|${NO_PROXY3}'
```
## Plugins
Generar archivo plugins.txt.
```
JENKINS_HOST=${JENKINS_USER}:${JENKINS_PASS}@${HOST}:${PORT}
JENKINS_PREFIX=${JENKINS_PREFIX_O_CONTEXTO}
curl -sSL "https://$JENKINS_HOST/$JENKINS_PREFIX/pluginManager/api/xml?depth=1&xpath=/*/*/shortName|/*/*/version&wrapper=plugins" | perl -pe 's/.*?<shortName>([\w-]+).*?<version>([^<]+)()(<\/\w+>)+/\1 \2\n/g'|sed 's/ /:/' > plugins.txt
```
## Skip Wizard
```
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
```
# Accessing and dumping Jenkins credentials
How Jenkins stores credentials
To access and decrypt Jenkins credentials you need three files.

 - credentials.xml - holds encrypted credentials
 - hudson.util.Secret - decrypts credentials.xml entries, this file is itself encrypted
 - master.key - decrypts hudson.util.Secret

All three files are located inside Jenkins home directory:
 - $JENKINS_HOME/credentials.xml 
 - $JENKINS_HOME/secrets/master.key
 - $JENKINS_HOME/secrets/hudson.util.Secret

Fuente: https://codurance.es/2019/05/30/accessing-and-dumping-jenkins-credentials/

## [Listing domains and credentials as XML](https://github.com/jenkinsci/credentials-plugin/blob/master/docs/user.adoc#listing-domains-and-credentials-as-xml)
La salida de este comando (list-credentials-as-xml), es la entrada para importar las credenciales con el comando: import-credentials-as-xml
```
java -jar jenkins-cli.jar -s https://${JENKINS_HOST}/${JENKINS_PREFIX}/ -auth ${JENKINS_USER}:${JENKINS_PASS} list-credentials-as-xml system::system::jenkins
```
## [Importing domains and credentials as XML](https://github.com/jenkinsci/credentials-plugin/blob/master/docs/user.adoc#importing-domains-and-credentials-as-xml)
```
java -jar jenkins-cli.jar -s https://${JENKINS_HOST}/${JENKINS_PREFIX}/ -auth ${JENKINS_USER}:${JENKINS_PASS} import-credentials-as-xml system::system::jenkins < credentials.xml
```