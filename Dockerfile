# Jenkins LTS Release Line: https://www.jenkins.io/download/lts/#lts-release-line
FROM jenkins/jenkins:lts

# Configuración del Proxy a nivel del Container
ENV HTTP_PROXY=${PROXY_HOST:PORT}
ENV HTTPS_PROXY=${PROXY_HOST:PORT}
ENV FTP_PROXY=${PROXY_HOST:PORT}
ENV SFTP_PROXY=${PROXY_HOST:PORT}
ENV http_proxy=${PROXY_HOST:PORT}
ENV https_proxy=${PROXY_HOST:PORT}
ENV ftp_proxy=${PROXY_HOST:PORT}
ENV sftp_proxy=${PROXY_HOST:PORT}
ENV NO_PROXY=${NO_PROXY1},${NO_PROXY2} ... ${NO_PROXYX}

# Skip Wizard
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

# Jenkins PREFIX (contexto) 
ENV JENKINS_OPTS --prefix=/jenkins_lts-test

# Jenkins ADMIN (USER y PASS)
ENV JENKINS_USER jenkins
ENV JENKINS_PASS jenkins

# Setting update centers - MIRROR, mas info: https://github.com/jenkinsci/docker/issues/594
#ENV JENKINS_UC_DOWNLOAD 'http://ftp-nyc.osuosl.org/pub/jenkins'
ENV JENKINS_UC_DOWNLOAD 'http://ftp.osuosl.org/pub/jenkins/'

# Plugin Installation Manager Tool for Jenkins, mas info: https://github.com/jenkinsci/plugin-installation-manager-tool
COPY jenkins-plugin-manager-2.1.1.jar /usr/share/jenkins/ref/jenkins-plugin-manager-2.1.1.jar

# Custom Groovy Script (utiliza las variables: JENKINS_USER y JENKINS_PASS)
COPY custom.groovy /usr/share/jenkins/ref/init.groovy.d/custom.groovy

# Lista de plugins
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt

WORKDIR /usr/share/jenkins/ref

# mas info: https://www.jenkins.io/projects/gsoc/2019/plugin-installation-manager-tool-cli/
RUN java -jar jenkins-plugin-manager-2.1.1.jar -f /usr/share/jenkins/ref/plugins.txt --verbose